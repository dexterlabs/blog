import { get } from 'svelte/store'
import { edit as logEdit } from './Log.svelte'
import { caretStore } from './Position.svelte'

const recurse = (state, path, fn) => {
  if (path.length) {
    if (state instanceof Array) {
      const [key, ...otherPath] = path
      return [
        ...state.slice(0, key),
        recurse(state[key], otherPath, fn),
        ...state.slice(key+1),
      ]
    } else if (state instanceof Object) {
      const [key, ...otherPath] = path
      return {
        ...state,
        [key]: recurse(state[key], otherPath, fn)
      }
    } else {
      console.log('Unknown content type', state, value)
      return state
    }
  } else {
    return fn(state)
  }
}

export const actions = {
  insert(state, value) {
    const { tree, position } = get(caretStore)
    const path = tree.currentNode.$$path
    caretStore.update(state => ({
      ...state,
      position: position + value.length
    }))
    logEdit(`Type: ${value}`)

    // Insert {value} at {state[...path]} at {position}
    console.log('actions.insert', state, path, position, value)
    return recurse(state, path, (state) => {
      if (typeof state === "string") {
        return `${state.slice(0, position)}${value}${state.slice(position)}`
      } else {
        console.log("Insert at unknown type", state)
        return state
      }
    })
  },
  remove(state, { word, backwards }) {
    const { tree, position } = get(caretStore)
    const path = tree.currentNode.$$path
    
    return recurse(state, path, state => {
      if (typeof state === "string") {
        if (backwards) {

          if (word) {
            const [toDelete] = state.slice(0, position).match(/(\w*|.)\s*$/)

            logEdit(`Backspace: '${toDelete}'`)
            caretStore.update(state => ({
              ...state,
              position: position - toDelete.length
            }))
            return `${state.slice(0, position - toDelete.length)}${state.slice(position)}`
          }
          logEdit(`Backspace: '${state[position-1]}'`)
          caretStore.update(state => ({
            ...state,
            position: position - 1
          }))
          return `${state.slice(0, position - 1)}${state.slice(position)}`
        }
        if (word) {
          const [toDelete] = state.slice(position).match(/^\s*(\w*|.)/)
          logEdit(`Delete: '${toDelete}'`)

          return `${state.slice(0, position)}${state.slice(position + toDelete.length)}`
        }
        logEdit(`Delete: '${state[position]}'`)
        return `${state.slice(0, position)}${state.slice(position + 1)}`
      } else {
        console.log("Delete at unknown type", state)
        return state
      }
    })
  }
}
