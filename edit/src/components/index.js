import Paragraph from './Paragraph.svelte'
import List from './List.svelte'
import Text from './Text.svelte'
import Image from './Image.svelte'

export const components = new Map([
  ['paragraph', Paragraph],
  ['list', List],
  ['text', Text],
  ['image', Image],
])

import Content from './Content.svelte'
export { Content }