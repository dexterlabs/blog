# Svelte Editor <insert nice name> without contentEditable

- Custom caret browsing 
- Tree selections based on selection api
- Custom keymapped editing
  - Inserting text/nodes
  - Deleting text/nodes
  - Making selections
  - Copy - pasta
  - Drag n Drop

- Custom elements
  - Basic elements:
    - bold/italic/underline
    - list -> listitem
    - link (edit target and src attrs)
    - image (edit alt and src attrs)
    - Heading
    - Paragraphs
    - Tables
  - Custom elements:
    - Layouts
    - Add own element by making a svelte component
    - Add 

## Component api
- Manage selections
- Blocking element or inline
- content render (via svelte component)
- Create / edit state (via actions/commands)

## Rendering state
Follow tree structure and render components from the componentMap context

### Keystroke/Event (state) -> Action(state) -> state.execute(Command)
- CQRS (Command Query Responsibility Segregation)


## contentEditable='minimal'
(…) it makes sense to enable a new, simpler version of contentEditable that provides basic functionality only. For the sake of discussion, call it contentEditable=’minimal’. The functionality provided by the browser under contentEditable=’minimal’ would be as follows:

    * Caret drawing
    * Events such as Keyboard , Clipboard, Drag and Drop
    * Some keyboard input handling- caret movement, typing of characters including Input Method Editor input
    * Selection drawing and manipulation according to the new Selection API spec
From: https://medium.com/content-uneditable/fixing-contenteditable-1a9a5073c35d